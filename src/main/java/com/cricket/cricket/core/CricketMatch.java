package com.cricket.cricket.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CricketMatch {

    private Team team1;
    private Team team2;
    private int teamSize;
    private int over;
    private boolean matchOver;

}
