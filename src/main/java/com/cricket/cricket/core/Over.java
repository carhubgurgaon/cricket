package com.cricket.cricket.core;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Over {

    private int over; // over number
    private List<Ball> balls;
    private int run;
    private int extra;
}
