package com.cricket.cricket.core;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Team {

    private String name;
    private List<Player> players;
    private List<Over> overPlayed;
    private int wicket;
    private int run;
    private boolean iningOver;
    private int iningOrder;
    private int extra;
}
