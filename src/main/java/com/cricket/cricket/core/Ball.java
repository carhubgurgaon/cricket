package com.cricket.cricket.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ball {

    private int run;
    private boolean wide;
    private boolean noBall;
    private boolean wicket;

}
