package com.cricket.cricket.service;

import com.cricket.cricket.dto.BattingOrderRequest;
import com.cricket.cricket.dto.CricketRequest;
import com.cricket.cricket.dto.OverDataRequest;

import java.util.List;

public interface ICricketService {

    void initializeMatch(CricketRequest cricketRequest);

    void battingOrder(BattingOrderRequest order);

    void addOverData(OverDataRequest overData);
}
