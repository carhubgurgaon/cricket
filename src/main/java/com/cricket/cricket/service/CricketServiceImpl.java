package com.cricket.cricket.service;

import com.cricket.cricket.core.*;
import com.cricket.cricket.dto.BattingOrderRequest;
import com.cricket.cricket.dto.CricketRequest;
import com.cricket.cricket.dto.OverDataRequest;
import com.cricket.cricket.events.CheckTeamIning;
import com.cricket.cricket.events.DisplayScore;
import com.cricket.cricket.events.TeamData;
import com.cricket.cricket.storage.IStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Service
public class CricketServiceImpl implements ICricketService {

    @Autowired
    private IStorageService storageService;

    public static TeamData teamData = new TeamData();

    @Override
    public void initializeMatch(CricketRequest cricketRequest) {
        CricketMatch cricketMatch = new CricketMatch();
        cricketMatch.setOver(cricketRequest.getOver());
        cricketMatch.setTeamSize(cricketRequest.getNumberOfPlayer());
        cricketMatch.setTeam1(getTeam("Team1",cricketRequest.getNumberOfPlayer(),1));
        cricketMatch.setTeam2(getTeam("Team2",cricketRequest.getNumberOfPlayer(),2));
        teamData.addObserver(new CheckTeamIning());
        teamData.addObserver(new DisplayScore());
        storageService.addMatchData(cricketMatch);
    }

    private Team getTeam(String teamName,Integer numberOfPlayer,int iningOrder) {
        Team team = new Team();
        List<Player> team1Players = new LinkedList<>();
        for (Integer i = 0; i < numberOfPlayer; i++) {
            Player player = new Player();
            team1Players.add(player);
        }
        team.setName(teamName);
        team.setPlayers(team1Players);
        team.setIningOrder(iningOrder);
        return team;
    }

    @Override
    public void battingOrder(BattingOrderRequest order) {
        Team team = storageService.getTeam(order.getTeamName());
        if(Objects.nonNull(team)){
            AtomicInteger i = new AtomicInteger();
            order.getOrder().forEach(s -> {
                Player player = team.getPlayers().get(i.get());
                player.setName(s);
                player.setBattingOrder(i.getAndIncrement());
            });
        }
    }

    @Override
    public void addOverData(OverDataRequest overData) {
        Team team = storageService.getTeam(overData.getTeamName());
        if(Objects.nonNull(team)){
            Over over = new Over();
            over.setOver(overData.getOver()+1);
            List<Player> players = getCurrentPlayers(team.getPlayers());
            AtomicReference<Player> p1 = new AtomicReference<>(players.get(0));
            AtomicReference<Player> p2 = new AtomicReference<>(players.get(1));
            AtomicBoolean playerLeft = new AtomicBoolean(true);
            List<Ball> balls = new LinkedList<>();
            overData.getBalls().forEach(ball->{
                Ball currentBall = new Ball();
                Player strikePlayer = null;
                Player nonStriker = null;
                if(!p1.get().isOnStrike() && !p2.get().isOnStrike()){
                    p1.get().setOnStrike(true);
                    strikePlayer = p1.get();
                    nonStriker = p2.get();
                }else if(p1.get().isOnStrike()){
                    strikePlayer = p1.get();
                    nonStriker = p2.get();
                }else{
                    strikePlayer = p2.get();
                    nonStriker = p1.get();
                }
                if(ball.equalsIgnoreCase("W")){
                    currentBall.setWicket(true);
                    team.setWicket(team.getWicket()+1);
                    strikePlayer.setOnStrike(false);
                    strikePlayer.setOnGround(false);
                    strikePlayer.setOut(true);
                    strikePlayer.setBallPlayed(strikePlayer.getBallPlayed()+1);
                    Player newPlayer = getNextPlayer(strikePlayer.getBattingOrder()>nonStriker.getBattingOrder()?strikePlayer.getBattingOrder()+1:nonStriker.getBattingOrder()+1,team.getPlayers());
                    if(Objects.isNull(newPlayer)){
                        // no player left
                        strikePlayer = null;
                        playerLeft.set(false);
                    }else{
                        strikePlayer = newPlayer;
                        strikePlayer.setOnGround(true);
                        strikePlayer.setOnStrike(true);
                        if(p1.get().getName().equalsIgnoreCase(nonStriker.getName())){
                            p2.set(newPlayer);
                        }else{
                            p1.set(newPlayer);
                        }
                    }
                }else if (ball.equalsIgnoreCase("1W")){
                    team.setWicket(team.getWicket()+1);
                    team.setRun(team.getRun()+1);
                    over.setRun(over.getRun()+1);
                    currentBall.setRun(1);
                    strikePlayer.setBallPlayed(strikePlayer.getBallPlayed()+1);
                    strikePlayer.setOnGround(false);
                    strikePlayer.setRun(strikePlayer.getRun()+1);
                    strikePlayer.setOut(true);
                    strikePlayer.setOnStrike(false);
                    Player newPlayer = getNextPlayer(strikePlayer.getBattingOrder()>nonStriker.getBattingOrder()?strikePlayer.getBattingOrder()+1:nonStriker.getBattingOrder()+1,team.getPlayers());
                    if(Objects.isNull(newPlayer)){
                        // no player left
                        playerLeft.set(false);
                    }else{
                        strikePlayer = newPlayer;
                        strikePlayer.setOnGround(true);
                        strikePlayer.setOnStrike(true);
                        if(p1.get().getName().equalsIgnoreCase(nonStriker.getName())){
                            p2.set(newPlayer);
                        }else{
                            p1.set(newPlayer);
                        }
                    }
                }else if (ball.equalsIgnoreCase("2W")){
                    team.setWicket(team.getWicket()+1);
                    team.setRun(team.getRun()+2);
                    over.setRun(over.getRun()+2);
                    currentBall.setRun(2);
                    strikePlayer.setBallPlayed(strikePlayer.getBallPlayed()+1);
                    strikePlayer.setOnGround(false);
                    strikePlayer.setRun(strikePlayer.getRun()+2);
                    strikePlayer.setOut(true);
                    strikePlayer.setOnStrike(false);
                    Player newPlayer = getNextPlayer(strikePlayer.getBattingOrder()>nonStriker.getBattingOrder()?strikePlayer.getBattingOrder()+1:nonStriker.getBattingOrder()+1,team.getPlayers());
                    if(Objects.isNull(newPlayer)){
                        // no player left
                        playerLeft.set(false);
                    }else{
                        strikePlayer = nonStriker;
                        nonStriker = newPlayer;
                        nonStriker.setOnGround(true);
                        strikePlayer.setOnGround(true);
                        strikePlayer.setOnStrike(true);
                        if(p1.get().getName().equalsIgnoreCase(nonStriker.getName())){
                            p2.set(newPlayer);
                        }else{
                            p1.set(newPlayer);
                        }
                    }
                }
                else if(ball.equalsIgnoreCase("Wd")){
                    team.setRun(team.getRun()+1);
                    team.setExtra(team.getExtra()+1);
                    over.setExtra(over.getExtra()+1);
                    over.setRun(over.getRun()+1);
                    currentBall.setWide(true);
                }else{
                    Integer run = Integer.parseInt(ball);
                    currentBall.setRun(run);
                    team.setRun(team.getRun()+run);
                    strikePlayer.setRun(strikePlayer.getRun()+run);
                    strikePlayer.setBallPlayed(strikePlayer.getBallPlayed()+1);
                    over.setRun(over.getRun()+run);
                    if(run % 2 != 0){
                        strikePlayer.setOnStrike(false);
                        nonStriker.setOnStrike(true);
                    }else{
                        if(run == 4){
                            strikePlayer.setFour(strikePlayer.getFour()+1);
                        }else if (run == 6){
                            strikePlayer.setSix(strikePlayer.getSix()+1);
                        }
                    }
                }
                balls.add(currentBall);
                List<Ball> ballList = over.getBalls();
                if(CollectionUtils.isEmpty(ballList)){
                    ballList = new LinkedList<>();
                    ballList.add(currentBall);
                }else{
                    ballList.add(currentBall);
                }
                over.setBalls(ballList);
            });
            if(playerLeft.get()){
                p1.get().setOnStrike(!p1.get().isOnStrike());
                p2.get().setOnStrike(!p2.get().isOnStrike());
            }
            List<Over> overs = team.getOverPlayed();
            if(CollectionUtils.isEmpty(overs)){
                overs = new LinkedList<>();
                overs.add(over);
            }else{
                overs.add(over);
            }
            team.setOverPlayed(overs);
            teamData.notifyForTeam(team.getName());
        }
    }

    private Player getNextPlayer(int order,List<Player> players) {
        List<Player> p =  players.stream().filter(player -> !player.isOut() && player.getBattingOrder() == order).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(p)){
            return null;
        }
        return p.get(0);
    }

    private List<Player> getCurrentPlayers(List<Player> players){
        Map<Integer,Player> map =  players.stream().sorted((o1, o2) -> {
             if(o1.getBattingOrder() > o2.getBattingOrder()){
                 return 1;
             }
            if(o1.getBattingOrder() < o2.getBattingOrder()){
                return -1;
            }
            return 0;
        }).filter(player -> !player.isOut()).collect(toMap(Player::getBattingOrder, Function.identity()));
        List<Player> currentPlayer = new ArrayList<>();
        for (Integer order : map.keySet()) {
            Player player = map.get(order);
            player.setOnGround(true);
            currentPlayer.add(player);
            if(currentPlayer.size() == 2){
                break;
            }
        }
        return currentPlayer;
    }
}
