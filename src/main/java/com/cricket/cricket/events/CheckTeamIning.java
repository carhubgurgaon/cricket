package com.cricket.cricket.events;

import com.cricket.cricket.core.CricketMatch;
import com.cricket.cricket.core.Over;
import com.cricket.cricket.core.Team;
import com.cricket.cricket.storage.Storage;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class CheckTeamIning implements Observer {

    private String teamName;

    @Override
    public void update(Observable o, Object name) {
        this.teamName = (String)name;
        checkTeamIning();
    }

    private void checkTeamIning() {
        Team team = Storage.getTeam(teamName);
        CricketMatch cricketMatch = Storage.getMatchData();
        if(team.getWicket() == (cricketMatch.getTeamSize()-1)){
            team.setIningOver(true);
        }else{

            double overPlayed = getOverPlayed(team.getOverPlayed());
            if(overPlayed == cricketMatch.getOver()){
                team.setIningOver(true);
            }
        }
        if(Storage.isMatchOver()){
            Set<String> teamNames = Storage.getTeamsName();
            teamNames.stream().forEach(s -> {
                if(!s.equalsIgnoreCase(team.getName())){
                    Team team2 = Storage.getTeam(s);
                    if(team2.getRun() > team.getRun()){
                        if(team2.getIningOrder() > team.getIningOrder()){
                            System.out.println("Result "+team2.getName()+ " Won by "+(cricketMatch.getTeamSize()-team2.getWicket())+" wickets");
                        }else{
                            System.out.println("Result "+team.getName()+ " Won by "+(team2.getRun()-team.getRun())+" runs");
                        }
                    }else if(team2.getRun() < team.getRun()){
                        if(team2.getIningOrder() > team.getIningOrder()){
                            System.out.println("Result "+team.getName()+ " Won by "+(team.getRun()-team2.getRun())+" runs");
                        }else{
                            System.out.println("Result "+team2.getName()+ " Won by "+(cricketMatch.getTeamSize()-team2.getWicket())+" wickets");
                        }
                    }else{
                        System.out.println("Result draw.");
                    }
                }
            });

        }
    }

    private double getOverPlayed(List<Over> overPlayed){
        Double overs = new Double(0.0);
        for (Over over : overPlayed) {
            int numberOfBalls = over.getBalls().stream().filter(ball -> !ball.isWide()).collect(toList()).size();
            String round = String.valueOf(Math.round(numberOfBalls / 6));
            String mod=String.valueOf(numberOfBalls%6);
            double numberOfOvers=Double.valueOf(round+"."+mod);
            overs = overs+numberOfOvers;
        }
        return overs;
    }
}
