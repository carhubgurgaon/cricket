package com.cricket.cricket.events;

import com.cricket.cricket.core.Over;
import com.cricket.cricket.core.Player;
import com.cricket.cricket.core.Team;
import com.cricket.cricket.storage.Storage;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static java.util.stream.Collectors.toList;


public class DisplayScore implements Observer {

    private String teamName;

    @Override
    public void update(Observable o, Object name) {
        this.teamName = (String)name;
        displayScore();
    }

    public void displayScore() {
        Team team = Storage.getTeam(teamName);
        System.out.println("Scorecard for "+teamName);
        System.out.println("Player Name \t\t"+"Score\t\t"+"4s\t\t"+"6s\t\t"+"Balls");
        List<Player> players = team.getPlayers().stream().sorted((o1, o2) -> {
            if(o1.getBattingOrder() > o2.getBattingOrder()){
                return 1;
            }
            if(o1.getBattingOrder() < o2.getBattingOrder()){
                return -1;
            }
            return 0;
        }).collect(toList());
        players.forEach(player -> {
            StringBuilder name = new StringBuilder(player.getName());
            if(player.isOnGround()){
                name.append("*");
            }
            System.out.println("\t\t"+name.toString()+"\t\t\t"+player.getRun()+"\t\t"+player.getFour()+"\t\t"+player.getSix()+"\t\t"+player.getBallPlayed());
        });
        System.out.println("Total:"+team.getRun()+"/"+team.getWicket());
        System.out.println("Extra:"+team.getExtra());
        System.out.println("Overs:"+getOverPlayed(team.getOverPlayed()));
    }

    private double getOverPlayed(List<Over> overPlayed){
        Double overs = new Double(0.0);
        for (Over over : overPlayed) {
            int numberOfBalls = over.getBalls().stream().filter(ball -> !ball.isWide()).collect(toList()).size();
            String round = String.valueOf(Math.round(numberOfBalls / 6));
            String mod=String.valueOf(numberOfBalls%6);
            double numberOfOvers=Double.valueOf(round+"."+mod);
            overs = overs+numberOfOvers;
        }
        return overs;
    }
}
