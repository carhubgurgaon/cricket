package com.cricket.cricket.events;

import java.util.Observable;

public class TeamData extends Observable {

    private String teamName;

    public void notifyForTeam(String teamName) {
        this.teamName = teamName;
        setChanged();
        notifyObservers(teamName);
    }



}
