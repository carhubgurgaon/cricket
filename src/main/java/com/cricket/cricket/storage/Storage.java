package com.cricket.cricket.storage;

import com.cricket.cricket.core.CricketMatch;
import com.cricket.cricket.core.Team;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Storage {

    public static Map<String,Team> teamData = new HashMap<>(); // teams data
    public static CricketMatch matchData = null; // match data

    public static void addMatchData(CricketMatch data) {
        matchData = data;
        Team t1 = data.getTeam1();
        addTeamData(t1.getName(),t1);
        Team t2 = data.getTeam2();
        addTeamData(t2.getName(),t2);
    }

    public static CricketMatch getMatchData() {
        return matchData;
    }

    public static void addTeamData(String teamName, Team team) {
        teamData.put(teamName,team);
    }

    public static Team getTeam(String teamName) {
        return teamData.get(teamName);
    }

    public static boolean isMatchOver(){
        Team team1 = matchData.getTeam1();
        Team team2 = matchData.getTeam2();
        return team1.isIningOver() && team2.isIningOver();
    }

    public static Set<String> getTeamsName(){
        return teamData.keySet();
    }
}
