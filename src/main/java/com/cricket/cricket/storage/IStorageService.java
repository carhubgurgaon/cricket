package com.cricket.cricket.storage;

import com.cricket.cricket.core.CricketMatch;
import com.cricket.cricket.core.Team;

public interface IStorageService {

    void addMatchData(CricketMatch cricketMatch);

    Team getTeam(String teamName);
}
