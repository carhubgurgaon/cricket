package com.cricket.cricket.storage;

import com.cricket.cricket.core.CricketMatch;
import com.cricket.cricket.core.Team;
import org.springframework.stereotype.Service;

@Service
public class StorageServiceImpl implements IStorageService{

    @Override
    public void addMatchData(CricketMatch cricketMatch) {
        Storage.addMatchData(cricketMatch);
    }

    @Override
    public Team getTeam(String teamName) {
        return Storage.getTeam(teamName);
    }
}
