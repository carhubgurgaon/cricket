package com.cricket.cricket.web.controller;

import com.cricket.cricket.dto.BattingOrderRequest;
import com.cricket.cricket.dto.CricketRequest;
import com.cricket.cricket.dto.OverDataRequest;
import com.cricket.cricket.service.ICricketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/cricket")
public class CricketController {

    @Autowired
    private ICricketService cricketService;

    @PostMapping(value = "/start-match")
    public ResponseEntity<Void> initializeMatch(@RequestBody CricketRequest cricketRequest){
        cricketService.initializeMatch(cricketRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value = "/batting-order")
    public ResponseEntity<Void> battingOrder(@RequestBody BattingOrderRequest order){
        cricketService.battingOrder(order);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/over")
    public ResponseEntity<Void> addOverData(@RequestBody OverDataRequest overData){
        cricketService.addOverData(overData);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
